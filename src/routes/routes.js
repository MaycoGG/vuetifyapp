const express = require('express')
const router = express.Router()

const Task = require('../models/Task')

// Get all tasks
router.get('/', async (req, res) => {
    const tasks = await Task.find()
    res.json(tasks)
})

// Get an especific task
router.get('/:_id', async(req, res) => {
    const task = await Task.findById(req.params._id)
    res.send(task)
})

// Save a task
router.post('/', async (req, res) => {
    const task = new Task(req.body)
    await task.save()
    res.json({
        estado: 'Tarea guardada!'
    })
})

// Update a task
router.put('/:_id', async (req, res) => {
    await Task.findByIdAndUpdate(req.params._id, req.body)
    res.json({
        estado: 'Tarea actualizada!'
    })
})

// Delete a task
router.delete('/:_id', async (req, res) => {
    await Task.findByIdAndDelete(req.params._id)
    res.json({
        estado: "Tarea eliminada!"
    })

})
module.exports = router